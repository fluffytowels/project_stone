﻿uses System, System.IO;
uses GraphABC, formsABC;
uses Events;

const DEBUG = False;

const BAD_X_OR_Y = -99;

const VEL_SCALE = 10; 
const ARR_HEAD_ANGLE = 150;
const ARROW_WIDTH = 2;
const ARROW_ERASER_WIDTH = 3;

const FLAG_COLOR = clCyan;

const STEPS_PER_SECOND = 20;
const TIME_STEP = 1.0 / STEPS_PER_SECOND;

const noty=480;
var Prev_x, Prev_y: integer;
var Prev_ang, Prev_vel: Real;
  
type TFormSettings = record
  acc: Real :=1.5;
  v: Real :=20;
  t_limit: integer :=30;
  angle: Real :=45;
end;

var FormSettings : TFormSettings;

type TTargetSettings = record
  x: Real := 200;
  y: Real := 0;
  r: Real := 10;
  linewid: integer := 2;
  color1: System.Drawing.Color := clRed;
  color2: System.Drawing.Color := clBeige;
end;

var Target, InitTarget : TTargetSettings;

var Xmas, Ymas: real;
var X_scr_0, Y_scr_0: Real; 

CONST FIELD_WIDTH = 45;

const FLAG_H = 40;
const FLAG_W = 30;
const FLAG_H2 = 20;

var 
  acc := new RealField('Ускорение:', FIELD_WIDTH);
  f1 := new FlowBreak;
  v := new RealField('Начальная скорость:', FIELD_WIDTH);
  f2 := new FlowBreak;
  t_limit := new IntegerField('Предел времени:', FIELD_WIDTH);

  angle := new RealField('Начальный угол:', FIELD_WIDTH);
  f3 := new FlowBreak;

  tl := new TextLabel('Масштаб: ');
  f4 := new FlowBreak;
  f_xmas := new RealField('по X:', FIELD_WIDTH);
  f_ymas := new RealField('по Y:', FIELD_WIDTH);
  f5 := new FlowBreak;

  f_x_0 := new RealField('X_0:', FIELD_WIDTH);
  f_y_0 := new RealField('Y_0:', FIELD_WIDTH);
  f7 := new FlowBreak;


  f8 := new FlowBreak;
  f9 := new FlowBreak;
  s1 := new Space(20);
  
  t2 := new TextLabel('Мишень: ');
  t_x := new RealField('X:', FIELD_WIDTH);
  t_y := new RealField('Y:', FIELD_WIDTH);
  f11 := new FlowBreak;
  t_r := new RealField('радиус:', FIELD_WIDTH);
  f12 := new FlowBreak;
  
  ok := new Button('Вычислить');
  clear :=  new Button('Очистить');
  save_screen :=  new Button('Скриншот');

function x_to_screen(x: Real): Integer;
begin
  x_to_screen := Round(x * Xmas + X_scr_0); 
end;

function y_to_screen(y: Real): Integer;
begin
  y_to_screen := Round(noty - (y * Ymas + Y_scr_0)) 
end;

procedure draw_arrow_with_color(deg_ang, vel: Real; col: System.Drawing.Color);
begin
  if DEBUG then Writeln('draw_arrow_with_color(', deg_ang, ' ', vel, col,')');
  var x0 := x_to_screen(0.0);
  var y0 := y_to_screen(0.0);
  
  var rad_ang := DegToRad(deg_ang);
  var x1 := x_to_screen(vel * cos(rad_ang));
  var y1 := y_to_screen(vel * sin(rad_ang));

  
  SetPenColor(col);
  MoveTo(x0, y0);
  LineTo(x1, y1);
  
  var head := vel/VEL_SCALE;
  if DEBUG then Writeln('head: ', head);
  var x2 := x_to_screen(vel * cos(rad_ang) + head * cos(DegToRad(deg_ang + ARR_HEAD_ANGLE)));
  var y2 := y_to_screen(vel * sin(rad_ang) + head * sin(DegToRad(deg_ang + ARR_HEAD_ANGLE)));

  var x3 := x_to_screen(vel * cos(rad_ang) + head * cos(DegToRad(deg_ang - ARR_HEAD_ANGLE)));
  var y3 := y_to_screen(vel * sin(rad_ang) + head * sin(DegToRad(deg_ang - ARR_HEAD_ANGLE)));
  
  if DEBUG then Writeln(x1, ' ', y1, ' ', x2, ' ', y2);
  if DEBUG then Writeln(x3, ' ', y3);

  LineTo(x2, y2);
  LineTo(x3, y3);
  LineTo(x1, y1);
end;

procedure draw_axes;
begin
  draw_arrow_with_color(0, 200.0, clBlack);
  draw_arrow_with_color(90, 200.0, clBlack);
end;


procedure SetPenAndFillColor(color: System.Drawing.Color);
begin
    SetPenColor(color);
    SetBrushColor(color);
end;

procedure draw_target(t: TTargetSettings);
var x_scr, y_scr, r: integer;
begin
    x_scr := x_to_screen(t.x); 
    y_scr := y_to_screen(t.y);
    
    r := x_to_screen(t.r) - x_to_screen(0);
    
    for var i := Round(r/t.linewid) downto 1 do begin  
      if i mod 2 = 0 then
        SetPenAndFillColor(t.color1)
      else
        SetPenAndFillColor(t.color2)
      ;
      Circle(x_scr, y_scr, i*t.linewid);
    end;
    
end;

procedure draw_flag_XY(x_real, y_real: Real);
var x_scr, y_scr: integer;
begin
    x_scr := x_to_screen(x_real); 
    y_scr := y_to_screen(y_real);
    SetPenColor(FLAG_COLOR);
    MoveTo(x_scr, y_scr);
    LineTo(x_scr,          y_scr - FLAG_H);
    LineTo(x_scr + FLAG_W, y_scr - FLAG_H);
    LineTo(x_scr + FLAG_W, y_scr - FLAG_H + FLAG_H2);
    LineTo(x_scr,          y_scr - FLAG_H + FLAG_H2);
end;


procedure draw_flag(x_real: Real);
begin
    draw_flag_XY(x_real, 0.0);
end;

procedure draw_hit(x_real, y_real: Real);
begin
    draw_flag_XY(x_real, y_real);
end;

procedure draw_arrow;
begin
  if DEBUG then Writeln('draw_arrow');
  
  var savePenWidth := PenWidth;
  
  Pen.Width := ARROW_ERASER_WIDTH;
  draw_arrow_with_color(Prev_ang, Prev_vel, clWhite);

  draw_axes;
  
  Pen.Width := ARROW_WIDTH;
  var ang := angle.Value;
  var vel := v.Value;
  draw_arrow_with_color(ang, vel, clCrimson);
  
  Prev_ang := ang;
  Prev_vel := vel;
  
  Pen.Width := savePenWidth;
end;

procedure onClick; forward;

procedure KeyDown(key: integer);
var changed: boolean;
begin
  changed := true;
  case key of 
    VK_Left: angle.value := angle.value + 1;
    VK_Right: angle.value := angle.value - 1;
    VK_Up: v.value := v.value + 1;
    VK_Down: if v.value > 1 then v.value := v.value - 1;
    
    VK_Space: onClick;
    
    else changed := false;
  
  end;
  if changed then
    draw_arrow
  ;   
end;

procedure set_onChange_handlers;
begin
  OnKeyDown := KeyDown; 

  acc.TextChanged += draw_arrow;
  v.TextChanged += draw_arrow;
  angle.TextChanged += draw_arrow;
end;

procedure Draw(x, y: real; draw_circle_P: Boolean);
var x_scr, y_scr: integer;
begin
    x_scr := x_to_screen(x); 
    y_scr := y_to_screen(y);
    if (prev_X = BAD_X_OR_Y) and (Prev_y = BAD_X_OR_Y) then 
      begin 
      end
    else begin
      SetPenColor(clGreen);
      Line(Prev_x, Prev_y, x_scr, y_scr);
    end;
    Prev_x := x_scr;
    Prev_y := y_scr;
    
    if draw_circle_P then begin 
      SetPenColor(clRed);
		  Circle(x_scr, y_scr, 1);
		end;
end; 

function distance(x1, y1, x2, y2: real): real;
begin
  distance := Sqrt(Sqr(x1 - x2) + Sqr(y1 - y2));
end;

procedure throw(a, v0, angle, t_limit: REAL);
var x, y : integer;
  real_x, real_y : real;
begin
  
  draw_axes;
  
 var t:=0.0;
 var count := 0;
 
 var vx:real := v0*cos(angle);
 VAR vy := v0*sin(angle); 

  Prev_x := BAD_X_OR_Y; 
  Prev_y := BAD_X_OR_Y; 
	repeat 
		real_x := vx*t;
		real_y := vy*t-(a*t*t)/2;
		x := Round(real_x);
		y := Round(real_y);
		
		Draw(x, y, (count mod STEPS_PER_SECOND = 0 )); 
	  if DEBUG then writeln(y,' ',x,' ',t);

    t += TIME_STEP;
    count += 1;
	  if y < 0 then begin
	    draw_flag(x);  
	    TextOut(x_to_screen(x) + 2, y_to_screen(y) - FLAG_H, Round(x).ToString);
	    break;
	  end;
	  if distance(real_x, real_y, Target.x, Target.y) <= Target.r then begin
	    draw_hit(x, y);  
	    TextOut(x_to_screen(x) + 2, y_to_screen(y) - FLAG_H, Round(x).ToString);
	    break;
	  end;
	  
	until t >= t_limit;
end;

procedure init_form;
begin
  acc.Value := FormSettings.acc;
  v.Value := FormSettings.v;
  t_limit.Value := FormSettings.t_limit;
  angle.Value := FormSettings.angle;

  Prev_ang := FormSettings.angle;
  Prev_vel := FormSettings.v;

  t_x.Value := Target.x;
  t_y.Value := Target.y;
  t_r.Value := Target.r;
end;

procedure onClick;
begin
 
  Xmas := f_xmas.Value;
  Ymas := f_ymas.Value;
  
  X_scr_0 := f_x_0.Value;
  Y_scr_0 := f_y_0.Value;
  
  Target.x := t_x.Value;
  Target.y := t_y.Value;
  Target.r := t_r.Value;
  draw_target(Target);
  
  var g: Real := acc.Value;
  throw(g, v.Value, angle.Value * Pi/180 , REAL(t_limit.Value));

end;

procedure draw_interface;forward;
procedure onClearClick;
begin
  ClearWindow();
  draw_interface;
end;

procedure onSaveClick;
begin
  var now := DateTime.Now; 
  var filename: String := 'pic_' + 
      now.Year + now.Month + now.Day +
      '_' +
      now.Hour + now.Minute + now.Second +
      '.png';
      
  SaveWindow(filename);
end;

procedure draw_interface;
begin

  f_xmas.Value := Xmas;
  f_ymas.Value := Ymas;
  f_x_0.Value := X_scr_0;
  f_y_0.Value := Y_scr_0;

  ok.Click += onClick; 
  clear.Click += onClearClick; 
  save_screen.Click += onSaveClick;
  
   MainForm.Title := 'Визуализация';

  MainForm.SetSize(100,600);
  
    init_form;
    draw_axes;
    draw_arrow;
    draw_target(Target);
  
end;

procedure Read_from_IniFile;
begin

  var exe_name:String := System.Environment.GetCommandLineArgs[0];
  var ini_name := exe_name.Replace('.exe', '.ini'); 
 
    if NOT FileExists(ini_name) then exit;     
      
    var lines := &File.ReadAllLines(ini_name);
    foreach var line in lines do
    begin
        if not line.StartsWith('[') then
        begin
            if DEBUG then Writeln(line);
            var part := line.Split('=');
            if part.Length < 2 then
              continue
            ;
            if DEBUG then Writeln(part);
            var (key, val) := (part[0], part[1]); 
            case key of
              'Xmas'   : Xmas    := val.ToReal;
              'Ymas'   : Ymas    := val.ToReal;
              'X_scr_0': X_scr_0 := val.ToReal;
              'Y_scr_0': Y_scr_0 := val.ToReal;
              
              'acc'    : FormSettings.acc     :=  val.ToReal;
              't_limit': FormSettings.t_limit :=  val.ToInteger;
              'angle'  : FormSettings.angle   :=  val.ToReal;

              't_x': Target.x := val.ToReal;
              't_y': Target.y := val.ToReal;
              't_r': Target.r := val.ToReal;
              't_linewid': Target.linewid := val.ToInteger;              
            end;
            
            if key = 'v' then begin
              FormSettings.v :=  val.ToReal;
            end;  
        end;
    end;
end;

procedure Init_Vars;
begin
  Xmas := 1.0;
  Ymas := 1.0;
  X_scr_0 := 0;
  Y_scr_0 := 0;
  
  Target := InitTarget; 
  
  Read_from_IniFile;
end;

begin
  
  Init_Vars;
  
  Window.Left := 25;
 
 draw_interface;
  set_onChange_handlers;
end.